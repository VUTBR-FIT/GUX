/**
 * GUX - project01
 *
 * @author Lukáš Černý (xcerny63)
 * @file draw.c
 */

/*
 * Standard XToolkit and OSF/Motif include files.
 */
#include <X11/Intrinsic.h>
#include <Xm/Xm.h> 

/*
 * Public include files for widgets used in this file.
 */
#include <Xm/MainW.h> 
#include <Xm/Form.h> 
#include <Xm/Frame.h>
#include <Xm/DrawingA.h>
#include <Xm/MessageB.h>
#include <Xm/PushB.h>
#include <Xm/RowColumn.h>

/*
 * Common C library include files
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>

/*
 * Shared variables
 */

#define LINES_ALLOC_STEP	10	/* memory allocation stepping */

#define WINDOW_HEIGHT 600
#define WINDOW_WIDTH 800

/** Definitions of structures and constants */
enum colors {
    BLACK,
    RED,
    GREEN,
    BLUE
};
enum filled {
    ENABLED,
    DISABLED
};
enum styles {
    FULL,
    DASHED
};
enum widths {
    W_0_PT,
    W_3_PT,
    W_8_PT
};
enum objTypes {
	POINT,
	LINE,
	RECTANGLE,
	ELLIPSE
};
typedef struct obj {
	enum objTypes type;
	enum widths width;
    enum filled isFilled;
    enum styles isDashed;
    enum colors lineBG, lineFG, filledBG, filledFG;

	int x1, y1, x2, y2;		/* input points */
	GC context;
	struct obj *next, *prev;
} Obj;
struct objStack {
    Obj *first;
    Obj *last;
};
typedef struct {
    enum objTypes type;
    enum widths width;
    enum filled isFilled;
    enum styles isDashed;
    enum colors lineBG, lineFG, filledBG, filledFG;
    int x1, y1, x2, y2;		/* input points */
    int buttonState;	    /* input state */
} Setting;
Setting currentSetting;     /* Global var for current setting */
struct objStack objects;    /* Global var for saved objects */
bool colorsPixelInit = false;
Pixel colorsPixel[4];       /* Global var for generated colors */
Widget quitDialog;		    /* GUX: Display dialog before close application */
GC inputGC = 0;			    /* GC used for drawing current position */

/**
 * Generate default colors & and return color by argument
 * @param display
 * @param color Requested color
 * @return Color
 */
Pixel getColor(Widget display, enum colors color) {
    if (!colorsPixelInit) {
        colorsPixelInit = true;

        XColor temp;
        Colormap colorMap = DefaultColormap(XtDisplay(display), DefaultScreen(XtDisplay(display)));
        temp.red = temp.green = temp.blue = 0x00;
        if ((!XAllocColor(XtDisplay(display), colorMap, &temp))) {
            fprintf(stderr, "[ERROR]\tGET COLOR: %s\n", "Black");
        }
        colorsPixel[BLACK] = temp.pixel;
        temp.red = 0xFFFF;
        if ((!XAllocColor(XtDisplay(display), colorMap, &temp))) {
            fprintf(stderr, "[ERROR]\tGET COLOR: %s\n", "Red");
        }
        colorsPixel[RED] = temp.pixel;
        temp.red = 0x00; temp.green = 0xFFFF;
        if ((!XAllocColor(XtDisplay(display), colorMap, &temp))) {
            fprintf(stderr, "[ERROR]\tGET COLOR: %s\n", "Green");
        }
        colorsPixel[GREEN] = temp.pixel;
        temp.green = 0x00; temp.blue = 0xFFFF;
        if ((!XAllocColor(XtDisplay(display), colorMap, &temp))) {
            fprintf(stderr, "[ERROR]\tGET COLOR: %s\n", "Blue");
        }
        colorsPixel[BLUE] = temp.pixel;
    }
    return colorsPixel[color];
}

/**
 * Convert with to pixels
 * @param width Converted value
 * @return Count pixels
 */
int getWidth(enum widths width) {
    switch (width) {
        case W_0_PT:
            return 0;
        case W_8_PT:
            return 8;
        default:
            return 3;
    }
}

/**
 * Get line style full or dashed
 * @param style
 * @return Value for GUI
 */
int getLineStyle(enum styles style) {
    if (style == FULL) {
        return LineSolid;
    } else {
        return LineDoubleDash;
    }
}

/**
 * Draw object on drawArea
 * @param parent
 * @param type      Type of object
 * @param width     With of lines
 * @param isFilled  Object is filled
 * @param isDashed  Style of lines (borders)
 * @param lineBG    Background of line
 * @param lineFG    Foreground of line
 * @param filledBG  Backgound of filled object
 * @param filledFG  Foreground of filled object
 * @param context   Context of object
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 */
void callbackDrawObject(Widget parent, enum objTypes type, enum widths width, enum filled isFilled, enum styles isDashed,
                        enum colors lineBG, enum colors lineFG, enum colors filledBG, enum colors filledFG,
                        GC context, int x1, int y1, int x2, int y2);

/**
 * Create new object and save it to the stack
 *
 * @param widget
 */
void newObject(Widget widget);
/*
 * "InputLine" event handler
 */
/* ARGSUSED */
void inputObjEH(Widget w, XtPointer client_data, XEvent *event, Boolean *cont);

/*
 * "DrawObj" callback function
 */
void callbackDrawObj(Widget w, XtPointer client_data, XtPointer call_data);

/*
 * "Expose" callback function
 */
/* ARGSUSED */
void callbackExpose(Widget parent, XtPointer client_data, XtPointer call_data);

/*
 * "Clear" button callback function
 */
/* ARGSUSED */
void callbackClear(Widget w, XtPointer client_data, XtPointer call_data);

/*
 * "Quit" button callback function
 */
/* ARGSUSED */
void callbackQuit(Widget w, XtPointer client_data, XtPointer call_data) {
	XtManageChild(quitDialog);
}

/**
 * Callback for quit program
 * @param target
 * @param clientData
 * @param callData Clicked button
 */
void callbackEnd(Widget target, XtPointer clientData, XtPointer callData);

/**
 * Changed type of object
 * @param target
 * @param clientData Selected item (new value)
 * @param callData
 */
void callbackMenuType(Widget target, XtPointer clientData, XtPointer callData);

/**
 * Changed style of line
 * @param target
 * @param clientData Selected item (new value)
 * @param callData
 */
void callbackMenuStyle(Widget target, XtPointer clientData, XtPointer callData);

/**
 * Changed width of line
 * @param target
 * @param clientData Selected item (new value)
 * @param callData
 */
void callbackMenuWidth(Widget target, XtPointer clientData, XtPointer callData);

/**
 * Changed background color of line
 * @param target
 * @param clientData Selected item (new value)
 * @param callData
 */
void callbackMenuLineBG(Widget target, XtPointer clientData, XtPointer callData);

/**
 * Changed foreground of line
 * @param target
 * @param clientData Selected item (new value)
 * @param callData
 */
void callbackMenuLineFG(Widget target, XtPointer clientData, XtPointer callData);

/**
 * Changed mode of filling of object
 * @param target
 * @param clientData Selected item (new value)
 * @param callData
 */
void callbackMenuFilled(Widget target, XtPointer clientData, XtPointer callData);

/**
 * Changed background color of object
 * @param target
 * @param clientData Selected item (new value)
 * @param callData
 */
void callbackMenuFilledBG(Widget target, XtPointer clientData, XtPointer callData);

/**
 * Changed foreground color of object
 * @param target
 * @param clientData Selected item (new value)
 * @param callData
 */
void callbackMenuFilledFG(Widget target, XtPointer clientData, XtPointer callData);

/**
 * Generate quit dialog
 * @param parent Parent element
 */
void initQuitDialog(Widget parent);

/**
 * Generate menubar
 * @param parent    Parent widget
 * @param menuBar   Created menuBar
 */
void initMenuBar(Widget parent, Widget *menuBar);

/**
 * Generated pullDown menu for select type of created object
 * @param menu  Parent menu
 * @param order Index of pullDown button
 */
void initMenuType(Widget menu, int *order);

/**
 * Generated pullDown menu for select full or dashed line
 * @param menu  Parent menu
 * @param order Index of pullDown button
 */
void initMenuStyle(Widget menu, int *order);

/**
 * Generated pullDown menu for select with of line
 * @param menu  Parent menu
 * @param order Index of pullDown button
 */
void initMenuWidth(Widget menu, int *order);

/**
 * Generated pullDown menu for select background color of line
 * @param menu  Parent menu
 * @param order Index of pullDown button
 */
void initMenuLineBG(Widget menu, int *order);

/**
 * Generated pullDown menu for select foreground color of line
 * @param menu  Parent menu
 * @param order Index of pullDown button
 */
void initMenuLineFG(Widget menu, int *order);

/**
 * Generated pullDown menu for enable or disable filled object
 * @param menu  Parent menu
 * @param order Index of pullDown button
 */
void initMenuFilled(Widget menu, int *order);

/**
 * Generated pullDown menu for select background color of object
 * @param menu  Parent menu
 * @param order Index of pullDown button
 */
void initMenuFilledBG(Widget menu, int *order);

/**
 * Generated pullDown menu for select foreground color object
 * @param menu  Parent menu
 * @param order Index of pullDown button
 */
void initMenuFilledFG(Widget menu, int *order);

/**
 * Select radio button
 * @param parent Widget with radiobuttons
 * @param index  Index of radiobutton to select
 */
void setDefaultRadioButton(Widget parent, int index) {
	char strIndex[12];
	sprintf(strIndex, "button_%d", index);
	Widget temp;
	if ((temp = XtNameToWidget(parent, strIndex))) {
		XtVaSetValues(temp, XmNset, true, NULL);
	}
}

/**
 * Generate array with default colors
 * @param colors Generated array
 * @return Size of generated array
 */
int generateColors(XmString colors[]) {
    colors[0] = XmStringCreateSimple("Black");
    colors[1] = XmStringCreateSimple("Red");
    colors[2] = XmStringCreateSimple("Green");
    colors[3] = XmStringCreateSimple("Blue");
	return 4;
}

/**
 * Generate array of strings
 * @param result    Generated array
 * @param count     Size of array
 * @param ...       Values of array
 */
void generateStringList(XmString result[], int count, ...) {
	va_list vaList;
	va_start(vaList, count);
	char *temp;
	for (int i = 0; i < count; i++) {
		temp = va_arg(vaList, char *);
		printf("%d\t%s\n", i, temp);
		result[i] = XmStringCreateSimple(temp);
	}

	va_end(vaList);
}

/**
 * Free items of array
 * @param source    Array with items
 * @param count     Size of array
 */
void freeStringList(XmString source[], int count) {
	for (int i = 0; i < count; i++) {
		XmStringFree(source[i]);
	}
}

int main(int argc, char **argv) {
    XtAppContext app_context;
    Widget topLevel, mainWin, frame, drawArea, rowColumn, quitBtn, clearBtn, menuBar;

    /*
     * Register the default language procedure
     */
    XtSetLanguageProc(NULL, (XtLanguageProc)NULL, NULL);

    topLevel = XtVaAppInitialize(
      &app_context,		 	/* Application context */
      "Draw",				/* Application class */
      NULL, 0,				/* command line option list */
      &argc, argv,			/* command line args */
      NULL,				/* for missing app-defaults file */
      NULL);				/* terminate varargs list */

    mainWin = XtVaCreateManagedWidget(
      "mainWin",			/* widget name */
      xmMainWindowWidgetClass,		/* widget class */
      topLevel,				/* parent widget*/
      XmNcommandWindowLocation, XmCOMMAND_BELOW_WORKSPACE,
      NULL);				/* terminate varargs list */

    frame = XtVaCreateManagedWidget(
      "frame",				/* widget name */
      xmFrameWidgetClass,		/* widget class */
      mainWin,				/* parent widget */
      NULL);				/* terminate varargs list */

    drawArea = XtVaCreateManagedWidget(
      "drawingArea",			/* widget name */
      xmDrawingAreaWidgetClass,		/* widget class */
      frame,				/* parent widget*/
      XmNwidth, WINDOW_WIDTH,			/* set startup width */
      XmNheight, WINDOW_HEIGHT,			/* set startup height */
      NULL);				/* terminate varargs list */

/*
    XSelectInput(XtDisplay(drawArea), XtWindow(drawArea), 
      KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask
      | Button1MotionMask );
*/
        
    rowColumn = XtVaCreateManagedWidget(
      "rowColumn",			/* widget name */
      xmRowColumnWidgetClass,		/* widget class */
      mainWin,				/* parent widget */
      XmNentryAlignment, XmALIGNMENT_CENTER,	/* alignment */
      XmNorientation, XmHORIZONTAL,	/* orientation */
      XmNpacking, XmPACK_COLUMN,	/* packing mode */
      NULL);				/* terminate varargs list */
        
    clearBtn = XtVaCreateManagedWidget(
      "Clear",				/* widget name */
      xmPushButtonWidgetClass,		/* widget class */
      rowColumn,			/* parent widget*/
      NULL);				/* terminate varargs list */

    quitBtn = XtVaCreateManagedWidget(
      "Quit",				/* widget name */
      xmPushButtonWidgetClass,		/* widget class */
      rowColumn,			/* parent widget*/
      NULL);				/* terminate varargs list */

    // Init settings
    currentSetting.type = LINE;
    currentSetting.isDashed = FULL;
    currentSetting.isFilled = DISABLED;
    currentSetting.width = W_3_PT;
    currentSetting.lineBG = BLUE;
    currentSetting.lineFG = BLUE;
    currentSetting.filledBG = BLACK;
    currentSetting.filledFG = BLACK;
    currentSetting.buttonState = 0;

    // Init objects
    objects.first = objects.last = NULL;

	initQuitDialog(topLevel);
	initMenuBar(mainWin, &menuBar);

    XmMainWindowSetAreas(mainWin, menuBar, rowColumn, NULL, NULL, frame);

    XtAddCallback(drawArea, XmNinputCallback, callbackDrawObj, drawArea);
    XtAddEventHandler(drawArea, ButtonMotionMask, false, inputObjEH, NULL);
    XtAddCallback(drawArea, XmNexposeCallback, callbackExpose, drawArea);

    XtAddCallback(clearBtn, XmNactivateCallback, callbackClear, drawArea);
    XtAddCallback(quitBtn, XmNactivateCallback, callbackQuit, 0);

    XtRealizeWidget(topLevel);

    XtAppMainLoop(app_context);

    return 0;
}

void initQuitDialog(Widget parent) {
	quitDialog = XmCreateQuestionDialog(parent, "close", NULL, 0);
	XmString msg = XmStringCreateSimple("Do you want to close this program?"),
	yes = XmStringCreateSimple("YES"),
	no = XmStringCreateSimple("NO");

	XtVaSetValues(quitDialog,
			XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL,
			XmNmessageString, msg,
			XmNokLabelString, yes,
			XmNcancelLabelString, no,
			NULL);

	XmStringFree(msg);
	XmStringFree(yes);
	XmStringFree(no);

	XtAddCallback(quitDialog, XmNokCallback, callbackEnd, NULL);

	XtUnmanageChild(XmMessageBoxGetChild(quitDialog, XmDIALOG_HELP_BUTTON));
}
/*
 * INIT MENU START
 */
void initMenuBar(Widget parent, Widget *menuBar) {
	XmString items[8];
	generateStringList(items, 8, "Type", "Style", "Width",
			"Line background", "Line foreground",
			"Filled?",
			"Filled background", "Filled foreground");

    *menuBar = XmVaCreateSimpleMenuBar(
			parent,
			"menu_bar",
			XmVaCASCADEBUTTON, items[0], 0,
			XmVaCASCADEBUTTON, items[1], 0,
			XmVaCASCADEBUTTON, items[2], 0,
			XmVaCASCADEBUTTON, items[3], 0,
			XmVaCASCADEBUTTON, items[4], 0,
			XmVaCASCADEBUTTON, items[5], 0,
			XmVaCASCADEBUTTON, items[6], 0,
			XmVaCASCADEBUTTON, items[7], 0,
			NULL);

	freeStringList(items, 8);

	XtManageChild(*menuBar);
	int order = 0;
	initMenuType(*menuBar, &order);
	initMenuStyle(*menuBar, &order);
	initMenuWidth(*menuBar, &order);
	initMenuLineBG(*menuBar, &order);
	initMenuLineFG(*menuBar, &order);
	initMenuFilled(*menuBar, &order);
	initMenuFilledBG(*menuBar, &order);
	initMenuFilledFG(*menuBar, &order);
}
void initMenuType(Widget menu, int *order) {
	XmString items[4];
	generateStringList(items, 4, "Point", "Line", "Rectangle", "Ellipse");

	Widget subMenu = XmVaCreateSimplePulldownMenu(
			menu, "type_menu", (*order)++, callbackMenuType,
			XmVaRADIOBUTTON, items[0], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[1], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[2], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[3], 0, NULL, NULL,
			XmNradioBehavior, true,
			XmNradioAlwaysOne, true,
			NULL
	);

	freeStringList(items, 4);
	setDefaultRadioButton(subMenu, currentSetting.type);
	XtManageChild(subMenu);
}
void initMenuWidth(Widget menu, int *order) {
	XmString items[3];
	generateStringList(items, 3, "0 pt", "3 pt", "8 pt");

	Widget subMenu = XmVaCreateSimplePulldownMenu(
			menu, "width_menu", (*order)++, callbackMenuWidth,
			XmVaRADIOBUTTON, items[0], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[1], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[2], 0, NULL, NULL,
			XmNradioBehavior, true,
			XmNradioAlwaysOne, true,
			NULL
	);

	freeStringList(items, 3);
    setDefaultRadioButton(subMenu, currentSetting.width);
	XtManageChild(subMenu);
}
void initMenuStyle(Widget menu, int *order) {
	XmString items[2];
	generateStringList(items, 2, "Full", "Dashed");

	Widget subMenu = XmVaCreateSimplePulldownMenu(
			menu, "style_menu", (*order)++, callbackMenuStyle,
			XmVaRADIOBUTTON, items[0], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[1], 0, NULL, NULL,
			XmNradioBehavior, true,
			XmNradioAlwaysOne, true,
			NULL
	);

	freeStringList(items, 2);
    setDefaultRadioButton(subMenu, currentSetting.isDashed);
	XtManageChild(subMenu);
}
void initMenuLineBG(Widget menu, int *order) {
	XmString items[4];
	int size = generateColors(items);

	Widget subMenu = XmVaCreateSimplePulldownMenu(
			menu, "line_bg_menu", (*order)++, callbackMenuLineBG,
			XmVaRADIOBUTTON, items[0], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[1], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[2], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[3], 0, NULL, NULL,
			XmNradioBehavior, true,
			XmNradioAlwaysOne, true,
			NULL
	);

	freeStringList(items, size);
    setDefaultRadioButton(subMenu, currentSetting.lineBG);
	XtManageChild(subMenu);
}
void initMenuLineFG(Widget menu, int *order){
	XmString items[4];
	int size = generateColors(items);

	Widget subMenu = XmVaCreateSimplePulldownMenu(
			menu, "line_fg_menu", (*order)++, callbackMenuLineFG,
			XmVaRADIOBUTTON, items[0], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[1], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[2], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[3], 0, NULL, NULL,
			XmNradioBehavior, true,
			XmNradioAlwaysOne, true,
			NULL
	);

	freeStringList(items, size);
    setDefaultRadioButton(subMenu, currentSetting.lineFG);
	XtManageChild(subMenu);
}
void initMenuFilled(Widget menu, int *order) {
	XmString items[2];
	generateStringList(items, 2, "Enable filling", "Disable filling");

	Widget subMenu = XmVaCreateSimplePulldownMenu(
			menu, "filled_menu", (*order)++, callbackMenuFilled,
			XmVaRADIOBUTTON, items[0], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[1], 0, NULL, NULL,
			XmNradioBehavior, true,
			XmNradioAlwaysOne, true,
			NULL
	);

	freeStringList(items, 2);
    setDefaultRadioButton(subMenu, currentSetting.isFilled);
	XtManageChild(subMenu);
}
void initMenuFilledBG(Widget menu, int *order) {
	XmString items[4];
	int size = generateColors(items);

	Widget subMenu = XmVaCreateSimplePulldownMenu(
			menu, "filled_bg_menu", (*order)++, callbackMenuFilledBG,
			XmVaRADIOBUTTON, items[0], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[1], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[2], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[3], 0, NULL, NULL,
			XmNradioBehavior, true,
			XmNradioAlwaysOne, true,
			NULL
	);

	freeStringList(items, size);
    setDefaultRadioButton(subMenu, currentSetting.filledBG);
	XtManageChild(subMenu);
}
void initMenuFilledFG(Widget menu, int *order) {
	XmString items[4];
	int size = generateColors(items);

	Widget subMenu = XmVaCreateSimplePulldownMenu(
			menu, "filled_fg_menu", (*order)++, callbackMenuFilledFG,
			XmVaRADIOBUTTON, items[0], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[1], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[2], 0, NULL, NULL,
			XmVaRADIOBUTTON, items[3], 0, NULL, NULL,
			XmNradioBehavior, true,
			XmNradioAlwaysOne, true,
			NULL
	);

	freeStringList(items, size);
    setDefaultRadioButton(subMenu, currentSetting.filledFG);
	XtManageChild(subMenu);
}
/*
 * INIT MENU END
 */
/*
 * CALLBACKS START
 */
void callbackEnd(Widget target, XtPointer clientData, XtPointer callData) {
	XmAnyCallbackStruct *cbs = (XmAnyCallbackStruct *) callData;
	switch (cbs->reason) {
		case XmCR_OK:
			exit(EXIT_SUCCESS);
			break;
	}
}
void callbackMenuType(Widget target, XtPointer clientData, XtPointer callData) {
    int index = (*((int *) &clientData));
    switch (index) {
        case POINT:
        case RECTANGLE:
        case ELLIPSE:
            currentSetting.type = index;
            break;
        default:
            currentSetting.type = LINE;
            break;
    }
}
void callbackMenuStyle(Widget target, XtPointer clientData, XtPointer callData) {
    int index = (*((int *) &clientData));
    if (index == DASHED) {
        currentSetting.isDashed = DASHED;
    } else {
        currentSetting.isDashed = FULL;
    }
}
void callbackMenuWidth(Widget target, XtPointer clientData, XtPointer callData) {
    int index = (*((int *) &clientData));
    switch (index) {
        case W_0_PT:
        case W_8_PT:
            currentSetting.width = index;
            break;
        default:
            currentSetting.width = W_3_PT;
            break;
    }
}
void callbackMenuLineBG(Widget target, XtPointer clientData, XtPointer callData) {
    int index = (*((int *) &clientData));
    switch (index) {
        case BLACK:
        case RED:
        case GREEN:
            currentSetting.lineBG = index;
            break;
        default:
            currentSetting.lineBG = BLUE;
            break;
    }
}
void callbackMenuLineFG(Widget target, XtPointer clientData, XtPointer callData) {
    int index = (*((int *) &clientData));
    switch (index) {
        case BLACK:
        case RED:
        case GREEN:
            currentSetting.lineFG = index;
            break;
        default:
            currentSetting.lineFG = BLUE;
            break;
    }
}
void callbackMenuFilled(Widget target, XtPointer clientData, XtPointer callData) {
    int index = (*((int *) &clientData));
    if (index == ENABLED) {
        currentSetting.isFilled = ENABLED;
    } else {
        currentSetting.isFilled = DISABLED;
    }
}
void callbackMenuFilledBG(Widget target, XtPointer clientData, XtPointer callData) {
    int index = (*((int *) &clientData));
    switch (index) {
        case BLACK:
        case RED:
        case GREEN:
            currentSetting.filledBG = index;
            break;
        default:
            currentSetting.filledBG = BLUE;
            break;
    }
}
void callbackMenuFilledFG(Widget target, XtPointer clientData, XtPointer callData) {
    int index = (*((int *) &clientData));
    switch (index) {
        case BLACK:
        case RED:
        case GREEN:
            currentSetting.filledFG = index;
            break;
        default:
            currentSetting.filledFG = BLUE;
            break;
    }
}
void callbackExpose(Widget parent, XtPointer client_data, XtPointer call_data) {
    Obj *temp = objects.first;
    while(temp != NULL) {
        callbackDrawObject(parent, temp->type, temp->width, temp->isFilled, temp->isDashed,
                           temp->lineBG, temp->lineFG, temp->filledBG, temp->filledFG,
                           temp->context, temp->x1, temp->y1, temp->x2, temp->y2);
        temp = temp->next;
    }
}
void callbackDrawObj(Widget w, XtPointer client_data, XtPointer call_data) {
    XmDrawingAreaCallbackStruct *d = (XmDrawingAreaCallbackStruct *) call_data;
    switch (d->event->type) {
        case ButtonPress:
            if (d->event->xbutton.button == Button1) {
                currentSetting.buttonState = 1;
                currentSetting.x1 = d->event->xbutton.x;
                currentSetting.y1 = d->event->xbutton.y;
            }
            break;
        case ButtonRelease:
            if (d->event->xbutton.button == Button1) {
                currentSetting.x2 = d->event->xbutton.x;
                currentSetting.y2 = d->event->xbutton.y;
                newObject((Widget) client_data);
                currentSetting.buttonState = 0;
                XClearArea(XtDisplay(w), XtWindow(w), 0, 0, 0, 0, true);
            }
            break;
    }
}
void callbackClear(Widget w, XtPointer client_data, XtPointer call_data) {
    Widget wcd = (Widget) client_data;
    Obj *temp = objects.first;
    objects.first = objects.last = NULL;
    while(temp != NULL) {
        XFreeGC(XtDisplay(w), temp->context);
        temp = temp->next;
        free(temp);
    }
    XClearWindow(XtDisplay(wcd), XtWindow(wcd));
}
void callbackDrawObject(Widget parent, enum objTypes type, enum widths width, enum filled isFilled, enum styles isDashed,
                        enum colors lineBG, enum colors lineFG, enum colors filledBG, enum colors filledFG,
                        GC context, int x1, int y1, int x2, int y2) {
    int offset = 0; //! For center
    int w = 0, h = 0;
    Pixel bg;
    offset = getWidth(width) / 2.0;
    int _x1 = x1,
            _y1 = y1;
    switch (type) {
        case POINT:
            if (width == W_0_PT) {
                XDrawPoint(XtDisplay(parent), XtWindow(parent), context, x1, y1);
            } else {
                XFillArc(XtDisplay(parent), XtWindow(parent), context, x1 - offset, y1 - offset,
                         getWidth(width), getWidth(width), 0, 360 * 64);
            }
            break;
        case RECTANGLE:
            w = abs(x1 - x2);
            h = abs(y1 - y2);
            //! Find left-down corner
            if (x1 > x2) {
                _x1 = x2;
            }
            if (y1 > y2) {
                _y1 = y2;
            }
            if (isFilled == ENABLED) {
                XtVaGetValues(parent, XmNbackground, &bg, NULL);
                XSetForeground(XtDisplay(parent), context, getColor(parent, filledFG));
                XFillRectangle(XtDisplay(parent), XtWindow(parent), context, _x1 + offset, _y1 + offset,
                               w - offset, h - offset);
                XSetForeground(XtDisplay(parent), context, getColor(parent, lineFG));
            }
            XDrawRectangle(XtDisplay(parent), XtWindow(parent), context, _x1, _y1, w, h);
            break;
        case ELLIPSE:
            w = abs(x1 - x2);// a
            h = abs(y1 - y2);// b
            //! Find left-down corner
            if (x1 < x2) {
                _x1 -= w;
            } else {
                _x1 = x2;
            }
            if (y1 < y2) {
                _y1 -= h;
            } else {
                _y1 = y2;
            }
            if (isFilled == ENABLED) {
                XSetForeground(XtDisplay(parent), context, getColor(parent, filledFG));
                XFillArc(XtDisplay(parent), XtWindow(parent), context, _x1 + offset, _y1 + offset, (w - offset) * 2,
                         (h - offset) * 2, 0, 360 * 64);
                XSetForeground(XtDisplay(parent), context, getColor(parent, lineFG));
            }
            XDrawArc(XtDisplay(parent), XtWindow(parent), context, _x1 + offset, _y1 + offset, (w - offset) * 2,
                     (h - offset) * 2, 0, 360 * 64);
            break;
        default:
            XDrawLine(XtDisplay(parent), XtWindow(parent), context, x1, y1, x2, y2);
            break;
    }
}
/*
 * CALLBACKS END
 */

void newObject(Widget widget) {
    Obj *temp = (Obj *) malloc(sizeof(Obj));
    temp->next = NULL;
    temp->prev = objects.last;
    if (objects.first == NULL) {
        objects.first = objects.last = temp;
    } else {
        objects.last->next = temp;
        temp->prev = objects.last;
        objects.last = temp;
    }
    //! Configure object
    temp->type = currentSetting.type;
    temp->width = currentSetting.width;
    temp->x1 = currentSetting.x1;
    temp->y1 = currentSetting.y1;
    temp->x2 = currentSetting.x2;
    temp->y2 = currentSetting.y2;
    temp->isFilled = currentSetting.isFilled;
    temp->isDashed = currentSetting.isDashed;
    temp->lineBG = currentSetting.lineBG;
    temp->lineFG = currentSetting.lineFG;
    temp->filledBG = currentSetting.filledBG;
    temp->filledFG = currentSetting.filledFG;
    temp->context = XCreateGC(XtDisplay(widget), XtWindow(widget), 0, NULL);

    XSetBackground(XtDisplay(widget), temp->context, getColor(widget, currentSetting.lineBG));
    XSetForeground(XtDisplay(widget), temp->context, getColor(widget, currentSetting.lineFG));
    XSetLineAttributes(XtDisplay(widget), temp->context, getWidth(currentSetting.width),
            getLineStyle(currentSetting.isDashed), CapRound, JoinMiter);
}

void inputObjEH(Widget w, XtPointer client_data, XEvent *event, Boolean *cont) {
    if (currentSetting.buttonState) {
        if (!inputGC) {
            inputGC = XCreateGC(XtDisplay(w), XtWindow(w), 0, NULL);
            XSetFunction(XtDisplay(w), inputGC, GXxor);
        }

        XSetBackground(XtDisplay(w), inputGC, getColor(w, currentSetting.lineBG));
        XSetForeground(XtDisplay(w), inputGC, getColor(w, currentSetting.lineFG));
        XSetLineAttributes(XtDisplay(w), inputGC, getWidth(currentSetting.width),
                           getLineStyle(currentSetting.isDashed), CapRound, JoinMiter);

        if (currentSetting.buttonState > 1) {
            /* erase previous position */
            callbackDrawObject(w, currentSetting.type, currentSetting.width, currentSetting.isFilled, currentSetting.isDashed,
                               currentSetting.lineBG, currentSetting.lineFG, currentSetting.filledBG, currentSetting.filledFG,
                               inputGC, currentSetting.x1, currentSetting.y1, currentSetting.x2, currentSetting.y2);
        } else {
            /* remember first MotionNotify */
            currentSetting.buttonState = 2;
        }

        currentSetting.x2 = event->xmotion.x;
        currentSetting.y2 = event->xmotion.y;
        callbackDrawObject(w, currentSetting.type, currentSetting.width, currentSetting.isFilled, currentSetting.isDashed,
                           currentSetting.lineBG, currentSetting.lineFG, currentSetting.filledBG, currentSetting.filledFG,
                           inputGC, currentSetting.x1, currentSetting.y1, currentSetting.x2, currentSetting.y2);
    }
}