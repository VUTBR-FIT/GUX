/**
 * GUX - proj02 - Tetris
 * @author Lukáš Černý (xcerny63)
 */
#include "Board.h"
#include <cstdlib>
#include <random>

bool Board::init(cairo_t *cr) {
    if (this->isInitialized)
        return false;
    srand((unsigned) time(nullptr));
    GtkWidget *grid = gtk_grid_new();
    int rows = height / BOARD_CELL_SIZE;
    int cols = width / BOARD_CELL_SIZE;
    this->board = new int*[rows];
    for (int row = 0; row < rows; row++) {
        this->board[row] = new int[cols];
        for (int col = 0; col < cols; col++) {
            this->board[row][col] = EMPTY;
            cairo_set_source_rgb(cr, 1, 1, 1);
            cairo_rectangle(cr, col * BOARD_CELL_SIZE + 2,
                    row * BOARD_CELL_SIZE + 2,
                    BOARD_CELL_SIZE - 4,
                    BOARD_CELL_SIZE - 4);
            cairo_fill(cr);
        }
    }
    this->isInitialized = true;
    return true;
}
void Board::reDraw(cairo_t *cr) {
    this->draw(cr, this->getDrawingBoard(this->board));
}
void Board::draw(cairo_t *cr, int **board) {
    int rows = height / BOARD_CELL_SIZE;
    int cols = width / BOARD_CELL_SIZE;
    int tempRow, tempCol;
    enum BoardColor color = BoardColor::EMPTY;
    for (int row = rows - 1; row >= 0; row--) {
        for (int col = 0; col < cols; col++) {
            color = (enum BoardColor) board[row][col];
            switch (color) {
                case BoardColor::YELLOW:
                    cairo_set_source_rgb(cr, 1, 1, 0);
                    break;
                case BoardColor::BROWN:
                    cairo_set_source_rgb(cr, 139/255, 69/255, 19/255);
                    break;
                case BoardColor::ORANGE:
                    cairo_set_source_rgb(cr, 1, 69/255, 0);
                    break;
                case BoardColor::RED:
                    cairo_set_source_rgb(cr, 1, 0, 0);
                    break;
                case BoardColor::GREEN:
                    cairo_set_source_rgb(cr, 0, 1, 0);
                    break;
                case BoardColor::BLUE:
                    cairo_set_source_rgb(cr, 0, 0, 1);
                    break;
                case BoardColor::PINK:
                    cairo_set_source_rgb(cr, 1, 20/255, 147/255);
                    break;
                case BoardColor::EMPTY:
                default:
                    cairo_set_source_rgb(cr, 1, 1, 1);
                    break;
            }
            cairo_rectangle(cr, col * BOARD_CELL_SIZE + 2,
                            row * BOARD_CELL_SIZE + 2,
                            BOARD_CELL_SIZE - 4,
                            BOARD_CELL_SIZE - 4);
            cairo_fill(cr);
        }
    }
}
void Board::generateObject() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> interval(0, ObjectType::_COUNT_TYPE - 1);
    this->object = this->_rotateObject(
            (enum ObjectType) (interval(gen)),
            ObjectRotation::UP);
}
bool Board::moveDown() {
    this->object.row++;
    if (!this->checkCollisionBorder(this->board, this->object) || !this->checkCollision(this->board, this->object)) {
        if (this->object.row < 2) {
            this->finished = true;
        }
        this->object.row--;
        this->addObject(this->object);
        int rows = height / BOARD_CELL_SIZE;
        this->removeLines(rows-1);
        this->generateObject();
        return false;
    }
    return true;
}
bool Board::moveRight() {
    this->object.col++;
    if (!this->checkCollision(this->board, this->object) || !this->checkCollisionBorder(this->board, this->object)) {
        this->object.col--;
        return false;
    }
    return true;
}
bool Board::moveLeft() {
    this->object.col--;
    if (!this->checkCollision(this->board, this->object) || !this->checkCollisionBorder(this->board, this->object)) {
        this->object.col++;
        return false;
    }
    return true;

}
bool Board::rotate() {
    struct boardObject temp = this->_rotateObject(this->object.type, (enum ObjectRotation )((this->object.rotation +1) % ObjectRotation::_COUNT_ROTATION));
    temp.col = this->object.col;
    temp.row = this->object.row;

    if (!this->checkCollision(this->board, temp) || !this->checkCollisionBorder(this->board, temp)) {
        return false;
    }
    temp.shadowCol = this->object.shadowCol;
    temp.shadowRow = this->object.shadowRow;
    this->object = temp;
    return true;
}
bool Board::checkCollision(int **board, struct boardObject obj) {
    int rows = height / BOARD_CELL_SIZE;
    int cols = width / BOARD_CELL_SIZE;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            if (obj.data[i][j] == EMPTY || obj.row < 0 || (obj.col+j) >= cols)
                continue;
            if (board[obj.row+i][obj.col+j])
                return false;
        }
    }
    return true;
}
bool Board::checkCollisionBorder(int **board, struct boardObject obj) {
    int rows = height / BOARD_CELL_SIZE;
    int cols = width / BOARD_CELL_SIZE;
    if (obj.col < 0 || obj.row + 4 > rows)
        return false;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            if (!obj.data[i][j] || obj.row+i < 0)
                continue;
            if (board[obj.row+i][obj.col+j])
                return false;
        }
    }
    return true;
}
struct boardObject Board::_rotateObject(enum ObjectType type, enum ObjectRotation rotation) {
    struct boardObject temp;
    temp.type = type;
    temp.rotation = rotation;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            temp.data[i][j] = BoardColor::EMPTY;
        }
    }
    temp.col = 3;
    temp.row = -1;

    switch (type) {
        case BOAT:
            temp.color = BoardColor::BLUE;
            temp.data[2][1] = temp.data[3][1] = temp.color;
            if (rotation == ObjectRotation::UP) {
                temp.data[3][0] = temp.data[3][2] = temp.color;
            } else if (rotation == ObjectRotation::RIGHT) {
                temp.data[2][2] = temp.data[1][1] = temp.color;
            } else if (rotation == ObjectRotation::DOWN) {
                temp.data[2][0] = temp.data[2][2] = temp.color;
            } else {
                temp.data[1][1] = temp.data[2][0] = temp.color;
            }
            break;
        case EL_R:
            temp.color = BoardColor::GREEN;
            if (rotation == ObjectRotation::UP) {
                temp.data[2][2] = temp.data[3][0] = temp.data[3][1]
                        = temp.data[3][2] = temp.color;
            } else if (rotation == ObjectRotation::RIGHT) {
                temp.data[1][0] = temp.data[2][0] = temp.data[3][0]
                        = temp.data[3][1] = temp.color;
            } else if (rotation == ObjectRotation::DOWN) {
                temp.data[3][0] = temp.data[2][0] = temp.data[2][1]
                        = temp.data[2][2] = temp.color;
            } else {
                temp.data[1][1] = temp.data[2][1] = temp.data[3][1]
                        = temp.data[1][0] = temp.color;
            }
            break;
        case EL_L:
            temp.color = BoardColor::RED;
            if (rotation == ObjectRotation::UP) {
                temp.data[2][0] = temp.data[3][0] = temp.data[3][1]
                        = temp.data[3][2] = temp.color;
            } else if (rotation == ObjectRotation::RIGHT) {
                temp.data[1][0] = temp.data[2][0] = temp.data[3][0]
                        = temp.data[1][1] = temp.color;
            } else if (rotation == ObjectRotation::DOWN) {
                temp.data[3][2] = temp.data[2][0] = temp.data[2][1]
                        = temp.data[2][2] = temp.color;
            } else {
                temp.data[1][1] = temp.data[2][1] = temp.data[3][1]
                        = temp.data[3][0] = temp.color;
            }
            break;
        case SQUARE:
            temp.col = 4;
            temp.color = BoardColor::ORANGE;
            temp.data[2][0] = temp.data[2][1] = temp.data[3][0]
                    = temp.data[3][1] = temp.color;
            break;
        case LINE:
            temp.color = BoardColor::PINK;
            if (rotation == ObjectRotation::UP || rotation == ObjectRotation::DOWN) {
                temp.data[3][0] = temp.data[2][0] = temp.data[1][0]
                        = temp.data[0][0] = temp.color;
            } else {
                temp.data[2][0] = temp.data[2][1] = temp.data[2][2]
                        = temp.data[2][3] = temp.color;
            }
            break;
        case ZET_R:
            temp.color = BoardColor::BROWN;
            temp.data[2][0] = temp.data[2][1] = temp.color;
            if (rotation == ObjectRotation::UP || rotation == ObjectRotation::DOWN) {
                temp.data[3][1] = temp.data[3][2] = temp.color;
            } else {
                temp.data[1][1] = temp.data[3][0] = temp.color;
            }
            break;
        case ZET_L:
        default:
            temp.color = BoardColor::YELLOW;
            temp.data[3][1] = temp.data[2][1] = temp.color;
            if (rotation == ObjectRotation::UP || rotation == ObjectRotation::DOWN) {
                temp.data[2][2] = temp.data[3][0] = temp.color;
            } else {
                temp.data[1][0] = temp.data[2][0] = temp.color;
            }
            break;
    }
    return temp;
}
void Board::reset() {
    int rows = height / BOARD_CELL_SIZE;
    int cols = width / BOARD_CELL_SIZE;
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            this->board[row][col] = BoardColor::EMPTY;
        }
    }
    this->generateObject();
    this->finished = false;
    this->score = 0;
}
int **Board::getDrawingBoard(int **cels) {
    int **temp;
    int rows = height / BOARD_CELL_SIZE;
    int cols = width / BOARD_CELL_SIZE;
    temp = new int*[rows];
    for (int row = 0; row < rows; row++) {
        temp[row] = new int[cols];
        for (int col = 0; col < cols; col++) {
            temp[row][col] = cels[row][col];
        }
    }
    //TODO do {    } while (this->checkCollision())

    //!Add shadow to the board
    for (int i = 0; i < 4; i++) {
        if (this->object.row + i < 0)
            continue;
        for (int j = 0; j < 4; j++) {
            if (this->object.data[i][j]) {
            //TODO    temp[this->shadowRow + i][this->shadowCol + j] = SHADOW;
            }
        }
    }

    for (int i = 0; i < 4; i++) {
        if (this->object.row + i < 0)
            continue;
        for (int j = 0; j < 4; j++) {
            if (this->object.data[i][j] && (!temp[this->object.row + i][this->object.col + j] || temp[this->object.row + i][this->object.col + j] == SHADOW)) {
                temp[this->object.row + i][this->object.col + j] = this->object.data[i][j];
            }
        }
    }
    return temp;
}
void Board::addObject(struct boardObject obj) {
    for (int i = 0; i < 4; i++) {
        if (obj.row + i < 0)
            continue;
        for (int j = 0; j < 4; j++) {
            if (obj.data[i][j] != EMPTY
                && obj.data[i][j] != SHADOW) {
                this->board[obj.row + i][obj.col + j] = obj.color;
            }
        }
    }
}
void Board::removeLines(int start) {
    int cols = width / BOARD_CELL_SIZE;
    do {
        bool isFull = true;
        for (int j = 0; j < cols && isFull; ++j) {
            isFull = this->board[start][j] != EMPTY && this->board[start][j] != SHADOW;
        }
        for (int j = 0; j < cols && isFull; ++j) {
            this->board[start][j] = EMPTY;
        }
        for (int i = start; i > 0 && isFull; i--) {
            for (int j = 0; j < cols; ++j) {
                this->board[i][j] = this->board[i - 1][j];
            }
        }
        if (!isFull) {
            start--;
        } else {
            printf("aaa\n");
            score += 50;
        }
    } while(start > 0);
}