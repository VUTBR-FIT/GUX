/**
 * GUX - proj02 - Tetris
 * @author Lukáš Černý (xcerny63)
 */
#include <gtk/gtk.h>
#include <string>
#include "Board.h"

#define WINDOW_NAME "GUX TETRIS"
#define BOARD_WIDTH 400
#define BOARD_HEIGHT 600

GtkWidget *window, *scoreValue, *endDialog;
Board *board;
bool gameRunning = true;
guint timer;
guint interval;

/**
 * Generate menu
 * @return Created menu
 */
GtkWidget *initMenu();
/**
 * Initialize timer
 */
void reInitTimer();
/**
 * Start CALLBACKS
 */
void callbackShowEndDialog();

void callbackEndDialog(gint response);

void callbackPause(GtkWidget *widget, GdkEventKey *event, gpointer data);

void callbackKeyPress(GtkWidget *widget, GdkEventKey *event, gpointer data) {
    if (event->type != GDK_KEY_PRESS || !gameRunning || board->isFinished()) {
        return;
    } else {
        switch (event->keyval) {
            case GDK_KEY_Up:
                board->rotate();
                break;
            case GDK_KEY_Down:
                board->moveDown();
                break;
            case GDK_KEY_Left:
                board->moveLeft();
                break;
            case GDK_KEY_Right:
                board->moveRight();
                break;
            default:
                break;
        }
        gtk_widget_queue_draw_area(board->getWidget(), 0, 0, BOARD_WIDTH, BOARD_HEIGHT);
    }
}

void callbackDraw(GtkWidget *widget, cairo_t *cr, gpointer data) {
    if (!board->init(cr)) {
        std::string temp = std::to_string(board->getScore());
        gtk_label_set_text(GTK_LABEL(scoreValue), temp.c_str());
        board->reDraw(cr);
    } else {
        board->generateObject();
        board->reDraw(cr);
    }
}

gboolean callbackTime(GtkWidget *widget) {
    if (!widget || !gameRunning) {
        return false;
    } else {
        if (board->isFinished()) {
            callbackShowEndDialog(); // Show end dialog
        } else {
            board->moveDown();
        }
    }
    gtk_widget_queue_draw_area(board->getWidget(), 0, 0, BOARD_WIDTH, BOARD_HEIGHT);
    return true;
}

void callbackNewGame() {
    interval = 1000;
    reInitTimer();
    board->reset();
    gtk_widget_queue_draw_area(board->getWidget(), 0, 0, BOARD_WIDTH, BOARD_HEIGHT);
}

int main(int argc, char **argv) {
    GtkWidget *screenLayout;

    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), WINDOW_NAME);

    screenLayout = gtk_grid_new();
    g_object_set(screenLayout, "margin", 5, nullptr);
    gtk_grid_set_row_spacing(GTK_GRID(screenLayout), 5);
    gtk_grid_set_column_spacing(GTK_GRID(screenLayout), 5);

    GtkWidget *menuBar = initMenu();
    board = new Board(BOARD_WIDTH, BOARD_HEIGHT);

    GtkWidget *scoreLabel = gtk_label_new("Score: ");
    scoreValue = gtk_label_new("0");
    GtkWidget *pause = gtk_button_new_with_label("   Pause  ");
    gtk_label_set_width_chars(GTK_LABEL(scoreValue), 5);
    gtk_label_set_justify(GTK_LABEL(scoreLabel), GTK_JUSTIFY_LEFT);
    gtk_label_set_justify(GTK_LABEL(scoreValue), GTK_JUSTIFY_RIGHT);

    gtk_grid_attach(GTK_GRID(screenLayout), menuBar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(screenLayout), scoreLabel, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(screenLayout), scoreValue, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(screenLayout), pause, (BOARD_WIDTH / 50) - 2, 1, 2, 1);
    gtk_grid_attach(GTK_GRID(screenLayout), board->getWidget(), 0, 2, BOARD_WIDTH / 50, 1);

    // Add main layout
    gtk_container_add(GTK_CONTAINER(window), screenLayout);
    // Configure window
    gtk_window_set_resizable(GTK_WINDOW(window), false);
    gtk_widget_set_size_request(window, BOARD_WIDTH, BOARD_HEIGHT + gtk_widget_get_allocated_height(menuBar)
        + gtk_widget_get_allocated_height(scoreLabel));

    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), nullptr);
    g_signal_connect(window, "key_press_event", G_CALLBACK(callbackKeyPress), nullptr);
    g_signal_connect(pause, "clicked", G_CALLBACK(callbackPause), nullptr);
    g_signal_connect(G_OBJECT(board->getWidget()), "draw", G_CALLBACK(callbackDraw), nullptr);

    interval = 1000;
    reInitTimer();

    gtk_widget_show_all(window);
    gtk_main();

    return 0;
}

void callbackPause(GtkWidget *widget, GdkEventKey *event, gpointer data) {
    gameRunning = !gameRunning;
    if (gameRunning) {
        gtk_button_set_label(GTK_BUTTON(widget), "   Pause  ");
        reInitTimer();
    } else {
        gtk_button_set_label(GTK_BUTTON(widget), "Continue");
    }
    printf("GAME: %s\n", gameRunning?"RUN":"PAUSE");
}

GtkWidget *initMenu() {
    GtkWidget *menuBar = gtk_menu_bar_new();
    GtkWidget *menu = gtk_menu_new();

    GtkWidget *gameLabel = gtk_menu_item_new_with_label("Game");
    GtkWidget *newGame = gtk_menu_item_new_with_label("New Game");
    GtkWidget *quit = gtk_menu_item_new_with_label("Quit");

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(gameLabel), menu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), newGame);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), quit);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar), gameLabel);

    g_signal_connect(quit, "activate", G_CALLBACK(gtk_main_quit), nullptr);
    g_signal_connect(newGame, "activate", G_CALLBACK(callbackNewGame), nullptr);

    return menuBar;
}

void callbackShowEndDialog() {
    std::string temp = "Your score is ";
    temp.append(std::to_string(board->getScore()));
    temp.append(".\nTry again?");

    endDialog = gtk_message_dialog_new(GTK_WINDOW(window),
                                GTK_DIALOG_DESTROY_WITH_PARENT,
                                GTK_MESSAGE_QUESTION,
                                GTK_BUTTONS_YES_NO,
                                temp.c_str());
    gtk_window_set_title(GTK_WINDOW(endDialog), "GAME OVER");
    gint response = gtk_dialog_run(GTK_DIALOG(endDialog));
    gtk_widget_destroy(endDialog);
    callbackEndDialog(response);
}

void callbackEndDialog(gint response) {
    switch (response) {
        case GTK_RESPONSE_YES:
            callbackNewGame();
            break;
        default:
            gtk_main_quit();
            break;
    }
}

void reInitTimer() {
    if (timer != 0) {
        g_source_remove(timer);
        timer = 0;
    }
    timer = g_timeout_add(interval, (GSourceFunc) callbackTime, &window);
}
