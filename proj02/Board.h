/**
 * GUX - proj02 - Tetris
 * @author Lukáš Černý (xcerny63)
 */
#include <gtk/gtk.h>

#ifndef GUX_BOARD_H
#define GUX_BOARD_H

#define BOARD_CELL_SIZE 40
#define BOARD_CELL_WHITE cairo_set_source_rgb(cr, 1, 1, 1);

/**
 * Enum lists
 */
enum BoardColor {
    EMPTY,
    RED,
    GREEN,
    BLUE,
    PINK,
    ORANGE,
    YELLOW,
    BROWN,
    SHADOW,
    _COUNT_COLOR
};
enum ObjectType {
    BOAT,
    EL_R,
    EL_L,
    SQUARE,
    LINE,
    ZET_R,
    ZET_L,
    _COUNT_TYPE
};
enum ObjectRotation {
    UP,
    RIGHT,
    DOWN,
    LEFT,
    _COUNT_ROTATION
};
/**
 * Current object
 */
struct boardObject {
    int row, col;
    int shadowRow, shadowCol;
    enum BoardColor data[4][4];
    enum ObjectType type;
    enum BoardColor color;
    enum ObjectRotation rotation;
};
/**
 * Class represented game board
 */
class Board {
public:
    Board(int width, int height) {
        finished = false;
        this->width = width;
        this->height = height;
        this->isInitialized = false;
        widget = gtk_drawing_area_new();
        gtk_widget_set_size_request(widget, width, height);
        score = 0;
    }
    /**
     * Draw board
     * @param cr
     * @return Success of operation
     */
    bool init(cairo_t *cr);
    /**
     * Reset game
     */
    void reset();
    /**
     * Move current object to the left
     * @return Success of operation
     */
    bool moveLeft();
    /**
     * Move current object to the right
     * @return Success o operation
     */
    bool moveRight();
    /**
     * Move current object down
     * @return Success of operation
     */
    bool moveDown();
    /**
     * Rotate current object
     * @return Success of operation
     */
    bool rotate();
    /**
     * Check if current game is finished
     * @return True if game finished
     */
    bool isFinished() {
        return finished;
    }
    GtkWidget *getWidget() {
        return widget;
    }
    int getScore() {
        return score;
    }
    /**
     * Check collisions new object with objects on the board
     *
     * @param board Game board
     * @param obj Checked object
     * @return If not collision then true
     */
    bool checkCollision(int **board, struct boardObject obj);
    /**
     * Check colisions with borders
     * @param board Game board
     * @param obj Checked object
     * @return If not collision then true
     */
    bool checkCollisionBorder(int **board, struct boardObject obj);
    /**
     * Generate board and draw it
     * @param cr
     */
    void reDraw(cairo_t *cr);
    /**
     * Generate new object
     */
    void generateObject();
private:
    int score;
    GtkWidget *widget;
    struct boardObject object;
    int shadowRow;
    int shadowCol;
    bool isInitialized;
    int **board;
    int height;
    int width;
    bool finished;
    /**
     * Remove filled rows
     * @param start Start row
     */
    void removeLines(int start);
    /**
     * Generate object by type and rotation
     * @param type Type of generated object
     * @param rotation Rotation of created object
     * @return Generated object
     */
    struct boardObject _rotateObject(enum ObjectType type, enum ObjectRotation rotation);
    /**
     * Draw board on GUI
     * @param cr
     * @param board Game board to draw
     */
    void draw(cairo_t *cr, int **board);
    /**
     * Generate board with objects
     * @param board Current board
     * @return New board
     */
    int **getDrawingBoard(int **board);
    /**
     * Add current object to current board
     * @param obj Added object
     */
    void addObject(struct boardObject obj);
};
#endif //GUX_BOARD_H
